#pragma once
#include <functional>
#include <vector>
#include <memory>
#include <string>

class EventLoop;
class EventLoopThread;

class EventLoopThreadPool
{
public:
	typedef std::function<void(EventLoop*)> ThreadInitCallback;
	EventLoopThreadPool();
	~EventLoopThreadPool();

	void Init(EventLoop* baseLoop, int numThreads);
	void start(const ThreadInitCallback& cb = ThreadInitCallback());
	
	//valid after calling start()
	//round-robin
	EventLoop* getNextLoop();

private:
	EventLoop*	m_pBaseLoop;
	std::string m_strName;
	bool		m_bStarted;
	int			m_numThreads;
	int			m_next;
	std::vector<std::shared_ptr<EventLoopThread> >	m_vecThreads;
	std::vector<EventLoop*>							m_vecLoops;
};