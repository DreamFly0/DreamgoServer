#include "Acceptor.h"

#include "../base/logging.h"
#include "EventLoop.h"
#include "InetAddress.h"
#include "SocketsOps.h"

#include <fcntl.h>		//open
#include <unistd.h>		//close

Acceptor::Acceptor(EventLoop* loop, const InetAddress& listenAddr, bool reusePort)
	: m_pLoop(loop),
	  m_acceptSocket(sockets::createNonblockingOrDie()),
	  m_acceptChannel(loop, m_acceptSocket.fd()),
	  m_blistenning(false),
	  idleFd_(::open("/dev/null",O_RDONLY | O_CLOEXEC))
{
	assert(idleFd_ >= 0);
	m_acceptSocket.setReuseAddr(true);
	m_acceptSocket.setReusePort(reusePort);
	m_acceptSocket.bindAddress(listenAddr);
	m_acceptChannel.setReadCallback(
		std::bind(&Acceptor::handleRead, this));
}

Acceptor::~Acceptor()
{
	m_acceptChannel.disableAll();
	m_acceptChannel.remove();
	::close(idleFd_);
}

void Acceptor::listen()
{
	m_pLoop->assertInLoopThread();
	m_blistenning = true;
	m_acceptSocket.listen();
	m_acceptChannel.enableReading();
}

void Acceptor::handleRead()
{
	m_pLoop->assertInLoopThread();
	InetAddress peerAddr(0);
	int connfd = m_acceptSocket.accept(&peerAddr);
	if(connfd >= 0){
		if(m_newConnCallback){
			//m_newConnCallbackʵ��ָ��TcpServer::newConnection(int sockfd, const InetAddress& peerAddr)
			m_newConnCallback(connfd, peerAddr);
		}else{
			sockets::close(connfd);
		}
	}
	else
	{
		LOG_SYSERR << "IN Acceptor : handleRead";
		// Read the section named "The special problem of
		// accept()ing when you can't" in libev's doc.
		// By Marc Lehmann, author of livev.
		// The process already has the maximum number of files open.
		if (errno == EMFILE)
		{
			::close(idleFd_);
			idleFd_ = ::accept(m_acceptSocket.fd(), NULL, NULL);
			::close(idleFd_);
			idleFd_ = ::open("/dev/null", O_RDONLY | O_CLOEXEC);
		}
	}
}