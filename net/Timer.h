/* 一个timier就是一个定时器，每个定时器都需要有自己的超时回调函数，最后定时器都交由TimerQueue组织
 * TimerQueue负责超时通知定时器调用回调函数
 *
 *
 */

#ifndef NET_TIMER_H
#define NET_TIMER_H

#include "../base/timestamp.h"
#include "Callbacks.h"
#include <atomic>

/// Internal class for timer event.

class Timer
{
public:
	Timer(const TimerCallback& cb, Timestamp when, double interval)
	: callback_(cb),
	  expiration_(when),
	  interval_(interval),
	  repeat_(interval > 0.0),
	  sequence_(++s_numCreated)
	{ }

	void run() const
	{
		callback_();
	}

	Timestamp expiration() const  { return expiration_; }
	bool repeat() const { return repeat_; }

	void restart(Timestamp now);

	static int64_t numCreated() { return s_numCreated; }
	const int64_t sequence(){ return sequence_; }
private:
	const TimerCallback callback_;
	Timestamp expiration_;
	const double interval_;
	const bool repeat_;
	const int64_t sequence_;

	static std::atomic<int64_t> s_numCreated;
};

#endif  // MUDUO_NET_TIMER_H
