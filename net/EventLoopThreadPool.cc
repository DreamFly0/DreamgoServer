#include "EventLoop.h"
#include "EventLoopThread.h"
#include "EventLoopThreadPool.h"
#include "Callbacks.h"

#include <assert.h>
#include <stdio.h>

EventLoopThreadPool::EventLoopThreadPool()
: m_pBaseLoop(NULL),
  m_strName("ThreadPool"),
  m_bStarted(false),
  m_numThreads(0),
  m_next(0)
{
	
}

EventLoopThreadPool::~EventLoopThreadPool()
{
	// Don't delete loop, it's stack variable
}

void EventLoopThreadPool::Init(EventLoop* baseLoop, int numThreads)
{
	m_numThreads = numThreads;
	m_pBaseLoop = baseLoop;
}

void EventLoopThreadPool::start(const ThreadInitCallback& cb)
{
	assert(m_pBaseLoop);
	assert(!m_bStarted);
	m_pBaseLoop->assertInLoopThread();

	m_bStarted = true;
	for (int i = 0; i < m_numThreads; i++)
	{
		char buf[m_strName.size() + 32];
		snprintf(buf,sizeof buf, "%s#%d",m_strName.c_str(),i);		//此处的buf只是标记了一个EventLoopThread名字，后续没有用它
		std::shared_ptr<EventLoopThread> t(new EventLoopThread(cb, buf));
		m_vecThreads.push_back(t);
		m_vecLoops.push_back(t->startLoop());
	}
	if (m_numThreads == 0 && cb)
	{
		cb(m_pBaseLoop);
	}
}

//valid after calling start()
//round-robin
EventLoop* EventLoopThreadPool::getNextLoop()
{
	m_pBaseLoop->assertInLoopThread();
	assert(m_bStarted);
	EventLoop *loop = m_pBaseLoop;
	
	if (!m_vecLoops.empty())
	{
		loop = m_vecLoops[m_next++];
		if (static_cast<size_t>(m_next) >= m_vecLoops.size())
		{
			m_next = 0;
		}
	}

	return loop;
}