#include "DatabaseMysql.h"
#include "../base/logging.h"
#include <fstream>
#include <stdarg.h>
#include <sstream>
#include <thread>
CDatabaseMysql::CDatabaseMysql()
{
	m_pMysql = nullptr;
	m_bInit = false;
}


CDatabaseMysql::~CDatabaseMysql()
{
	if (m_pMysql != NULL)
	{
		if (m_bInit)
		{
			mysql_close(m_pMysql);
		}
	}
}

bool CDatabaseMysql::Initialize(const string & host, const string & user, const string & pwd, const string & dbname)
{
	stringstream ss;
	ss << this_thread::get_id();

	LOG_INFO << "CDatabaseMysql::Initialize，in threads: " << ss.str() <<" begin... " ;
	LOG_INFO << "mysql info: host=" << host << ", user=" << user << ", password=" << pwd << ", dbname=" << dbname;
	if (m_bInit)
	{
		mysql_close(m_pMysql);
	}

	m_pMysql = mysql_init(m_pMysql);
	m_pMysql = mysql_real_connect(m_pMysql, host.c_str(), user.c_str(),
		pwd.c_str(), dbname.c_str(), 0, nullptr, 0);

	m_DBInfo.strDBName = dbname;
	m_DBInfo.strHost = host;
	m_DBInfo.strUser = user;
	m_DBInfo.strPwd = pwd;

	if (m_pMysql)
	{
		mysql_query(m_pMysql, "set names utf8");
		m_bInit = true;
		return true;
	}
	else
	{
		LOG_ERROR << "Could not connect to MySQL database at " << host.c_str()
			<< ", " << mysql_error(m_pMysql);
		mysql_close(m_pMysql);
		return false;
	}

	LOG_INFO << "CDatabaseMysql::Initialize, init failed!";
	return false;
}

QueryResultPtr CDatabaseMysql::Query(const char * sql)
{
	if (!m_pMysql)
	{
		LOG_INFO << "CDatabaseMysql::Query, mysql is disconnected!";
		if (false == Initialize(m_DBInfo.strHost, m_DBInfo.strUser,
			m_DBInfo.strPwd, m_DBInfo.strDBName))
		{
			return nullptr;
		}
	}

	if (!m_pMysql)
		return nullptr;

	MYSQL_RES *result = nullptr;
	uint64_t rowCount = 0;
	uint32_t fieldCount = 0;
	
	{
		LOG_INFO << sql;
		int iTmpRet = mysql_real_query(m_pMysql, sql, strlen(sql));
		if (iTmpRet)
		{
			uint32_t uErrno = mysql_errno(m_pMysql);
			LOG_INFO << "CDatabaseMysql::Query, mysql is abnormal, errno : " << uErrno;
			if (CR_SERVER_GONE_ERROR == uErrno)
			{
				LOG_INFO << "CDatabaseMysql::Query, mysql is disconnected!";
				if (false == Initialize(m_DBInfo.strHost, m_DBInfo.strUser,
					m_DBInfo.strPwd, m_DBInfo.strDBName))
				{
					return nullptr;
				}
				LOG_INFO << sql;
				iTmpRet = mysql_real_query(m_pMysql, sql, strlen(sql));
				if (iTmpRet)
				{
					LOG_ERROR << "SQL: " << sql;
					LOG_ERROR << "query ERROR: " << mysql_error(m_pMysql);
				}
			}
			else
			{
				LOG_ERROR << "SQL: " << sql;
				LOG_ERROR << "query ERROR: " << mysql_error(m_pMysql);
				return NULL;
			}
		}
		result = mysql_store_result(m_pMysql);

		rowCount = mysql_affected_rows(m_pMysql);
		fieldCount = mysql_field_count(m_pMysql);
	}
	if (!result)
		return nullptr;

	//此处存在内存泄漏安全隐患
	//QueryResult *queryResult = new QueryResult(result, rowCount, fieldCount);
	QueryResultPtr queryResult(new QueryResult(result, rowCount, fieldCount));
	queryResult->NextRow();

	return queryResult;
}


QueryResultPtr CDatabaseMysql::PQuery(const char * format, ...)
{
	if (!format)
		return nullptr;
	va_list ap;
	char szQuery[MAX_QUERY_LEN];
	va_start(ap, format);
	int res = vsnprintf(szQuery, MAX_QUERY_LEN, format, ap);
	va_end(ap);

	if (res == -1)
	{
		LOG_ERROR << "SQL Query truncated (and not execute) for format: " << format;
		return NULL;
	}

	return Query(szQuery);

	return nullptr;
}

bool CDatabaseMysql::Execute(const char * sql)
{
	if (!m_pMysql)
		return false;

	{
		int iTempRet = mysql_query(m_pMysql, sql);
		if (iTempRet)
		{
			unsigned int uErrno = mysql_errno(m_pMysql);
			LOG_INFO << "CDatabaseMysql::Query, mysql is abnormal, errno : " << uErrno;
			if (CR_SERVER_GONE_ERROR == uErrno)
			{
				LOG_INFO << "CDatabaseMysql::Query, mysql is disconnected!";
				if (false == Initialize(m_DBInfo.strHost, m_DBInfo.strUser,
					m_DBInfo.strPwd, m_DBInfo.strDBName))
				{
					return false;
				}
				LOG_INFO << sql;
				iTempRet = mysql_real_query(m_pMysql, sql, strlen(sql));
				if (iTempRet)
				{
					LOG_ERROR << "SQL: " << sql;
					LOG_ERROR << "query ERROR: " << mysql_error(m_pMysql);
				}
			}
			else
			{
				LOG_ERROR << "SQL: " << sql;
				LOG_ERROR << "query ERROR: " << mysql_error(m_pMysql);
			}
			return false;
		}
	}

	return true;
}

bool CDatabaseMysql::Execute(const char * sql, uint32_t & uAffectedCount, int & nErrno)
{
	if (!m_pMysql)
		return false;

	{
		int iTempRet = mysql_query(m_pMysql, sql);
		if (iTempRet)
		{
			unsigned int uErrno = mysql_errno(m_pMysql);
			LOG_ERROR << "CDatabaseMysql::Query, mysql is abnormal, errno : " << uErrno;
			if (CR_SERVER_GONE_ERROR == uErrno)
			{
				LOG_ERROR << "CDatabaseMysql::Query, mysql is disconnected!";
				if (false == Initialize(m_DBInfo.strHost, m_DBInfo.strUser,
					m_DBInfo.strPwd, m_DBInfo.strDBName))
				{
					return false;
				}
				LOG_INFO << sql;
				iTempRet = mysql_query(m_pMysql, sql);
				nErrno = iTempRet;
				if (iTempRet)
				{
					LOG_ERROR << "SQL: " << sql;
					LOG_ERROR << "query ERROR: " << mysql_error(m_pMysql);
				}
			}
			else
			{
				LOG_ERROR << "SQL: " << sql;
				LOG_ERROR << "query ERROR: " << mysql_error(m_pMysql);
			}
			return false;
		}
		uAffectedCount = static_cast<uint32_t>(mysql_affected_rows(m_pMysql));
	}

	return true;
}

bool CDatabaseMysql::PExecute(const char * format, ...)
{
	if (!format)
		return false;

	va_list ap;
	char szQuery[MAX_QUERY_LEN];
	va_start(ap, format);
	int res = vsnprintf(szQuery, MAX_QUERY_LEN, format, ap);
	va_end(ap);

	if (res == -1)
	{
		LOG_ERROR << "SQL Query truncated (and not execute) for format: " << format;
		return false;
	}

	if (!m_pMysql)
		return false;

	{
		int iTempRet = mysql_query(m_pMysql, szQuery);
		if (iTempRet)
		{
			unsigned int uErrno = mysql_errno(m_pMysql);
			LOG_ERROR << "CDatabaseMysql::Query, mysql is abnormal, errno : " << uErrno;
			if (CR_SERVER_GONE_ERROR == uErrno)
			{
				LOG_ERROR << "CDatabaseMysql::Query, mysql is disconnected!";
				if (false == Initialize(m_DBInfo.strHost, m_DBInfo.strUser,
					m_DBInfo.strPwd, m_DBInfo.strDBName))
				{
					return false;
				}
				LOG_INFO << szQuery;
				iTempRet = mysql_query(m_pMysql, szQuery);
				if (iTempRet)
				{
					LOG_ERROR << "SQL: " << szQuery;
					LOG_ERROR << "query ERROR: " << mysql_error(m_pMysql);
				}
			}
			else
			{
				LOG_ERROR << "SQL: " << szQuery;
				LOG_ERROR << "query ERROR: " << mysql_error(m_pMysql);
			}
			return false;
		}
	}

	return true;
}

uint32_t CDatabaseMysql::GetInsertID()
{
	return (uint32_t)mysql_insert_id(m_pMysql);
}

void CDatabaseMysql::ClearStoredResults()
{
	if (!m_pMysql)
	{
		return;
	}

	MYSQL_RES* result = NULL;
	while (!mysql_next_result(m_pMysql))
	{
		if ((result = mysql_store_result(m_pMysql)) != NULL)
		{
			mysql_free_result(result);
		}
	}
}

int32_t CDatabaseMysql::EscapeString(char * szDst, const char * szSrc, uint32_t uSize)
{
	if (m_pMysql == NULL)
	{
		return 0;
	}
	if (szDst == NULL || szSrc == NULL)
	{
		return 0;
	}

	return mysql_real_escape_string(m_pMysql, szDst, szSrc, uSize);
}
