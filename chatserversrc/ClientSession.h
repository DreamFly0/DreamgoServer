/******************************************************************
Copyright (C) 2018 - All Rights Reserved by
文 件 名 : ClientSession.h --- ClientSession
编写日期 : 2018/12/7
说 明    : 负责传输层数据业务处理---装包和解包，每一个连接都有唯一一个ClientSession
历史纪录 :
<作者>    <日期>        <版本>        <内容>
*******************************************************************/
#pragma once
#include "../net/Buffer.h"
#include "../net/TimerId.h"
#include "TcpSession.h"

struct OnlineUserInfo
{
	int32_t     userid;
	std::string username;
	std::string nickname;
	std::string password;
	int32_t     clienttype;     //客户端类型,pc=1, android=2, ios=3
	int32_t     status;         //在线状态 0离线 1在线 2忙碌 3离开 4隐身
};

//业务处理类，这个类负责消息的解析，分发
class ClientSession : public TcpSession
{
public:
	ClientSession(const std::shared_ptr<TcpConnection>& tmpConn);
	virtual ~ClientSession();

	ClientSession(const ClientSession& rhs) = delete;
	ClientSession& operator =(const ClientSession& rhs) = delete;

	void OnRead(const TcpConnectionPtr& conn, Buffer* pBuffer, Timestamp receiveTime);

	int32_t GetUserId()
	{
		return m_userinfo.userid;
	}
	/**
	*@param type 取值： 1 用户上线； 2 用户下线； 3 个人昵称、头像、签名等信息更改
	*/
	void SendUserStatusChangeMsg(int32_t userid, int type);

	void EnableHeartbeatCheck();
	void DisableHeartbeatCheck();

	//检测心跳包，如果指定时间内（现在是30秒）未收到数据包，则主动断开于客户端的连接
	void CheckHeartbeat(const std::shared_ptr<TcpConnection>& conn);
private:
	bool Process(const std::shared_ptr<TcpConnection>& conn, const char* inbuf, size_t length);

	void OnHeartbeatResponse(const TcpConnectionPtr conn);
	void OnRegisterResponse(const std::string& data, const std::shared_ptr<TcpConnection>& conn);
	void OnLoginResponse(const std::string& data, const std::shared_ptr<TcpConnection>& conn);
	void OnGetFriendListResponse(const std::shared_ptr<TcpConnection>& conn);
	void OnUpdateUserInfoResponse(const std::string& data, const std::shared_ptr<TcpConnection>& conn);
	void OnFindUserResponse(const std::string& data, const std::shared_ptr<TcpConnection>& conn);
	void OnOperateFriendResponse(const std::string& data, const std::shared_ptr<TcpConnection>& conn);
	void OnChatResponse(int32_t targetid, const std::string& data, const std::shared_ptr<TcpConnection>& conn);
	void OnModifyPasswordResponse(const std::string& data, const std::shared_ptr<TcpConnection>& conn);

	void DeleteFriend(const std::shared_ptr<TcpConnection>& conn, int32_t friendid);
private:
	int32_t					m_id;					//session id
	OnlineUserInfo			m_userinfo;				//记录该session的用户信息
	int						m_seq;					//当前Session数据包序列号
	time_t					m_lastPackageTime;      //上一次收发包的时间
	TimerId					m_checkOnlineTimerId; //检测是否在线的定时器id
};