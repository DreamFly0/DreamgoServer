#include "ClientSession.h"
#include "../base/logging.h"
#include "../base/singleton.h"
#include "../net/protocolstream.h"
#include "../net/TcpConnection.h"
#include "../jsoncpp-0.5.0/reader.h"
#include "../jsoncpp-0.5.0/value.h"
#include "../jsoncpp-0.5.0/writer.h"
#include "UserManager.h"
#include "Msg.h"
#include "IMServer.h"
#include "MsgCacheManager.h"

#include <cstring>
#include <time.h>

//允许的最大时数据包来往间隔，这里设置成30秒
#define MAX_NO_PACKAGE_INTERVAL  30

ClientSession::ClientSession(const std::shared_ptr<TcpConnection>& tmpConn) :
TcpSession(tmpConn),
m_id(0),
m_seq(0)
{
	m_userinfo.userid = 0;
	m_lastPackageTime = time(NULL);

	//暂且注释掉，不利于调试
	EnableHeartbeatCheck();
}

ClientSession::~ClientSession()
{
	LOG_INFO << "~ClientSession";
}

//+------------+--------------------------------------------------------------------------------------+
//|    包头    |								数据								                  |
//+------------+------------------+--------------------+----------+----------+------------------------+
//|msg(struct) |   PACKLEN(int)   |CHECKSUM_LEN(short) | cmd(int) | seq(int) | data(datalen+data)     |
//+------------+------------------+--------------------+----------+----------+------------------------+
void ClientSession::OnRead(const TcpConnectionPtr& conn, Buffer* pBuffer, Timestamp receiveTime)
{
	while (true)
	{
		//不够一个包头大小
		if (pBuffer->readableBytes() < (size_t)sizeof(msg))
		{
			LOG_INFO << "buffer is not enough for a package header, pBuffer->readableBytes()=" << pBuffer->readableBytes() << ", sizeof(msg)=" << sizeof(msg);
			return;
		}
		//不够一个整包大小
		msg header;
		memcpy(&header, pBuffer->peek(), sizeof(msg));
		if (pBuffer->readableBytes() < (size_t)sizeof(msg)+(size_t)header.packagesize)
		{
			LOG_INFO << "buffer is not enough for a package";
			return;
		}

		pBuffer->retrieve(sizeof(msg));
		std::string inbuf;
		inbuf.append(pBuffer->peek(), header.packagesize);
		pBuffer->retrieve(header.packagesize);
		if (!Process(conn, inbuf.c_str(), inbuf.length()))
		{
			LOG_WARN << "Process error,close TcpConnection";
			conn->forceClose();
		}

		m_lastPackageTime = time(NULL);
	}// end while-loop
}

void ClientSession::SendUserStatusChangeMsg(int32_t userid, int type)
{
	LOG_INFO << "USERID = " << userid;
	string data;
	//用户上线
	if (type == 1)
	{
		data = "{\"type\": 1, \"onlinestatus\": 1}";
	}
	//用户下线
	else if (type == 2)
	{
		data = "{\"type\": 2, \"onlinestatus\": 0}";
	}
	//个人昵称、头像、签名等信息更改
	else if (type == 3)
	{
		Json::Value jsonData;
		Json::Value jsonObj;
		//个人信息打包
		User user;
		Singleton<UserManager>::Instance().GetUserInfoByUserId(userid, user);
		jsonObj["userid"] = user.userid;
		jsonObj["username"] = user.username;
		jsonObj["nickname"] = user.nickname;
		jsonObj["facetype"] = user.facetype;
		jsonObj["customface"] = user.customface;
		jsonObj["gender"] = user.gender;
		jsonObj["birthday"] = user.birthday;
		jsonObj["signature"] = user.signature;
		jsonObj["address"] = user.address;
		jsonObj["phonenumber"] = user.phonenumber;
		jsonObj["mail"] = user.mail;
		jsonObj["clienttype"] = 1;
		jsonObj["status"] = m_userinfo.status;

		jsonData["type"] = 3;
		jsonData["userinfo"] = jsonObj;
		Json::FastWriter writer;
		data = writer.write(jsonData);
	}

	std::string outbuf;
	yt::BinaryWriteStream3 writeStream(&outbuf);
	writeStream.Write(msg_type_userstatuschange);
	writeStream.Write(m_seq);
	writeStream.Write(data.c_str(), data.length());
	writeStream.Write(userid);
	writeStream.Flush();

	Send(outbuf);

	LOG_INFO << "Send to client: cmd=msg_type_userstatuschange, data=" << data << ", userid=" << m_userinfo.userid;
}

void ClientSession::EnableHeartbeatCheck()
{
	std::shared_ptr<TcpConnection> conn = GetConnectionPtr();
	if (conn)
	{
		m_checkOnlineTimerId = conn->getLoop()->runEvery(5, std::bind(&ClientSession::CheckHeartbeat, this, conn));
	}
}

void ClientSession::DisableHeartbeatCheck()
{
	std::shared_ptr<TcpConnection> conn = GetConnectionPtr();
	if (conn)
	{
		LOG_INFO << "remove check online timerId, userid=" << m_userinfo.userid
			<< ", clientType=" << m_userinfo.clienttype
			<< ", client address: " << conn->peerAddress().toHostPort();
		conn->getLoop()->cancel(m_checkOnlineTimerId);
	}
}

void ClientSession::CheckHeartbeat(const std::shared_ptr<TcpConnection>& conn)
{
	if (!conn)
		return;
	if (time(NULL) - m_lastPackageTime < MAX_NO_PACKAGE_INTERVAL)
		return;

	conn->forceClose();
	LOG_INFO << "in max no-package time, no package, close the connection, userid=" << m_userinfo.userid
		<< ", clientType=" << m_userinfo.clienttype
		<< ", client address: " << conn->peerAddress().toHostPort();
}

//收到一个message以后，inbuf指向去掉msg头以后的message,length是数据长度
bool ClientSession::Process(const std::shared_ptr<TcpConnection>& conn, const char* inbuf, size_t length)
{
	yt::BinaryReadStream2 readStream(inbuf, length);
	int cmd;
	if (!readStream.Read(cmd))
	{
		LOG_ERROR << "READ CMD ERROR!!!";
		return false;
	}
	if (!readStream.Read(m_seq))
	{
		LOG_WARN << "READ SEQ ERROR!!!";
		return false;
	}
	std::string data;
	size_t dataLength;
	if (!readStream.Read(&data, 0, dataLength))
	{
		LOG_WARN << "READ DATA ERROR!!!";
		return false;
	}

	LOG_INFO << "Recv from client: cmd=" << cmd << ", seq=" << m_seq << ", header.packagesize:" << length << ", data=" << data << ", datalength=" << dataLength;
	LOG_DEBUG_BIN((unsigned char*)inbuf, length);

	switch (cmd)
	{
		//心跳包
		case msg_type_heartbeart:
		{
			OnHeartbeatResponse(conn);
		}
			break;

		//注册账号
		case msg_type_register:
		{
			OnRegisterResponse(data, conn);
		}
			break;

		//登录鉴权
		case msg_type_login:
		{
			OnLoginResponse(data, conn);
		}
			break;
		//获取好友列表
		case msg_type_getofriendlist:
		{
			OnGetFriendListResponse(conn);
		}
			break;

		//用户个人信息更新
		case msg_type_updateuserinfo:
		{
			OnUpdateUserInfoResponse(data, conn);
		}
			break;

		//查找好友请求
		case msg_type_finduser:
		{
			OnFindUserResponse(data, conn);
		}
			break;

		//操作好友
		case msg_type_operatefriend:
		{
			OnOperateFriendResponse(data, conn);
		}
			break;

		//聊天消息
		case msg_type_chat:
		{
			int32_t target;
			if (!readStream.Read(target))
			{
				LOG_WARN << "read target error !!!";
				return false;
			}
			OnChatResponse(target, data, conn);
		}
		break;

		//修改密码：
		case msg_type_modifypassword:
		{
			OnModifyPasswordResponse(data, conn);
		}
			break;
		default:
			LOG_WARN << "unsupport cmd, cmd:" << cmd << ", data=" << data << ", connection name:" << conn->peerAddress().toHostPort();
			return false;

	}//end switch
	++m_seq;
	return true;
}

void ClientSession::OnHeartbeatResponse(const TcpConnectionPtr conn)
{
	std::string outbuf;
	yt::BinaryWriteStream3 writeStream(&outbuf);
	writeStream.Write(msg_type_heartbeart);
	writeStream.Write(m_seq);
	std::string dummy;
	writeStream.Write(dummy.c_str(), dummy.length());
	writeStream.Flush();
	LOG_INFO << "Response to client: cmd=1000" << ", sessionId=" << m_id;
	Send(outbuf);
}

//客户端发来注册消息，应答
void ClientSession::OnRegisterResponse(const std::string & data, const std::shared_ptr<TcpConnection>& conn)
{
	//{ "user": "13917043329", "nickname" : "balloon", "password" : "123" }
	Json::Reader JsonReader;
	Json::Value JsonRoot;
	if (!JsonReader.parse(data, JsonRoot))
	{
		LOG_WARN << "invalid json: " << data << ", sessionId = " << m_id << ", client: " << conn->peerAddress().toHostPort();
		return;
	}

	if (!JsonRoot["username"].isString() || !JsonRoot["nickname"].isString() || !JsonRoot["password"].isString())
	{
		LOG_WARN << "invalid json: " << data << ", sessionId = " << m_id << ", client: " << conn->peerAddress().toHostPort();
		return;
	}

	User u;
	u.username = JsonRoot["username"].asString();
	u.nickname = JsonRoot["username"].asString();
	u.password = JsonRoot["password"].asString();

	string retData;
	User cachedUser;
	cachedUser.userid = 0;
	Singleton<UserManager>::Instance().GetUserInfoByUsername(u.username, cachedUser);
	if (cachedUser.userid != 0)
	{
		Json::Value arrayObj;   // 构建对象
		arrayObj["code"] = 101;
		arrayObj["msg"] = "registered already";
		Json::FastWriter writer;
		retData = writer.write(arrayObj);
	}
	else
	{
		if (!Singleton<UserManager>::Instance().AddUser(u))
		{
			Json::Value arrayObj;   // 构建对象
			arrayObj["code"] = 101;
			arrayObj["msg"] = "registered already";
			Json::FastWriter writer;
			retData = writer.write(arrayObj);
		}
		else
		{
			Json::Value arrayObj;   // 构建对象
			arrayObj["code"] = 0;
			arrayObj["msg"] = "ok";
			Json::FastWriter writer;
			retData = writer.write(arrayObj);
		}
	}

	std::string outbuf;
	yt::BinaryWriteStream3 writeStream(&outbuf);
	writeStream.Write(msg_type_register);
	writeStream.Write(m_seq);
	writeStream.Write(retData.c_str(), retData.length());
	writeStream.Flush();

	LOG_INFO << "Response to client: cmd=msg_type_register" << ", userid=" << u.userid << ", data=" << retData;

	Send(outbuf);
}

void ClientSession::OnLoginResponse(const std::string & data, const std::shared_ptr<TcpConnection>& conn)
{
	//{"username": "13917043329", "password": "123", "clienttype": 1, "status": 1}
	Json::Reader JsonReader;
	Json::Value JsonRoot;

	if (!JsonReader.parse(data, JsonRoot))
	{
		LOG_WARN << "invalid json: " << data << ", sessionId = " << m_id << ", client: " << conn->peerAddress().toHostPort();
		return;
	}

	if (!JsonRoot["username"].isString() || !JsonRoot["password"].isString() || !JsonRoot["clienttype"].isInt() || !JsonRoot["status"].isInt())
	{
		LOG_WARN << "invalid json: " << data << ", sessionId = " << m_id << ", client: " << conn->peerAddress().toHostPort();
		return;
	}

	string username = JsonRoot["username"].asString();
	string password = JsonRoot["password"].asString();
	string retData;
	User cachedUser;
	cachedUser.userid = 0;
	Singleton<UserManager>::Instance().GetUserInfoByUsername(username, cachedUser);
	if (0 == cachedUser.userid)
	{
		Json::Value jsonObj;   // 构建对象
		jsonObj["code"] = 102;
		jsonObj["msg"] = "not registered";
		Json::FastWriter writer;
		retData = writer.write(jsonObj);
	}
	else
	{
		if (cachedUser.password != password)
		{
			Json::Value jsonObj;   // 构建对象
			jsonObj["code"] = 103;
			jsonObj["msg"] = "incorrect password";
			Json::FastWriter writer;
			retData = writer.write(jsonObj);
		}
		else
		{
			//记录用户信息
			m_userinfo.userid = cachedUser.userid;
			m_userinfo.username = username;
			m_userinfo.nickname = cachedUser.nickname;
			m_userinfo.password = password;
			m_userinfo.clienttype = JsonRoot["clienttype"].asInt();
			m_userinfo.status = JsonRoot["status"].asInt();

			Json::Value jsonObj;   // 构建对象
			jsonObj["code"] = 0;
			jsonObj["msg"] = "ok";
			jsonObj["userid"] = m_userinfo.userid;
			jsonObj["username"] = cachedUser.username;
			jsonObj["nickname"] = cachedUser.nickname;
			jsonObj["facetype"] = cachedUser.facetype;
			jsonObj["customface"] = cachedUser.customface;
			jsonObj["gender"] = cachedUser.gender;
			jsonObj["birthday"] = cachedUser.birthday;
			jsonObj["signature"] = cachedUser.signature;
			jsonObj["address"] = cachedUser.address;
			jsonObj["phonenumber"] = cachedUser.phonenumber;
			jsonObj["mail"] = cachedUser.mail;

			Json::FastWriter writer;
			retData = writer.write(jsonObj);
		}
	}
	//登录信息应答
	string outbuf;
	yt::BinaryWriteStream3 writeStream(&outbuf);
	writeStream.Write(msg_type_login);
	writeStream.Write(m_seq);
	writeStream.Write(retData.c_str(), retData.length());
	writeStream.Flush();

	LOG_INFO << "Response to client: cmd=msg_type_login, data=" << retData << ", userid=" << m_userinfo.userid;

	Send(outbuf);

	//推送通知消息
	std::list<NotifyMsgCache> listNotifyCache;
	Singleton<MsgCacheManager>::Instance().GetNotifyMsgCache(m_userinfo.userid, listNotifyCache);
	for (const auto& iter : listNotifyCache)
	{
		Send(iter.notifymsg);
	}

	//推送聊天消息
	std::list<ChatMsgCache> listChatCache;
	Singleton<MsgCacheManager>::Instance().GetChatMsgCache(m_userinfo.userid, listChatCache);
	for (const auto &iter : listChatCache)
	{
		Send(iter.chatmsg);
	}

	//给其他用户推送上线消息
	std::list<User> friends;
	Singleton<UserManager>::Instance().GetFriendInfoByUserId(m_userinfo.userid,friends);
	IMServer& imserver = Singleton<IMServer>::Instance();
	for (const auto& iter : friends)
	{
		//先看目标用户是否在线
		std::shared_ptr<ClientSession> targetSession;
		imserver.GetSessionByUserId(targetSession, iter.userid);
		if (targetSession.get() != nullptr)
			targetSession->SendUserStatusChangeMsg(m_userinfo.userid, 1);
	}
}

void ClientSession::OnGetFriendListResponse(const std::shared_ptr<TcpConnection>& conn)
{
	std::list<User> friends;
	Singleton<UserManager>::Instance().GetFriendInfoByUserId(m_userinfo.userid, friends);
	std::string strUserInfo;
	bool userOnline = false;
	IMServer& imserver = Singleton<IMServer>::Instance();
	Json::Value jsonObj;
	Json::Value jsonArray;   // 构建对象
	
	int i = 0;
	for (const auto& iter : friends)
	{
		userOnline = imserver.IsUserSessionExsit(iter.userid);
		jsonArray[i]["userid"] = iter.userid;
		jsonArray[i]["username"] = iter.username;
		jsonArray[i]["nickname"] = iter.nickname;
		jsonArray[i]["facetype"] = iter.facetype;
		jsonArray[i]["customface"] = iter.customface;
		jsonArray[i]["gender"] = iter.gender;
		jsonArray[i]["birthday"] = iter.birthday;
		jsonArray[i]["signature"] = iter.signature;
		jsonArray[i]["address"] = iter.address;
		jsonArray[i]["phonenumber"] = iter.phonenumber;
		jsonArray[i]["mail"] = iter.mail;
		jsonArray[i]["clienttype"] = 1;
		jsonArray[i]["status"] = (userOnline ? 1 : 0);
		i++;
	}

	jsonObj["code"] = 0;
	jsonObj["msg"] = "ok";
	jsonObj["userinfo"] = jsonArray;
	Json::FastWriter writer;
	string Data = writer.write(jsonObj);

	std::string outbuf;
	yt::BinaryWriteStream3 writeStream(&outbuf);
	writeStream.Write(msg_type_getofriendlist);
	writeStream.Write(m_seq);
	writeStream.Write(Data.c_str(), Data.length());
	writeStream.Flush();

	LOG_INFO << "Response to client: cmd=msg_type_getofriendlist, data=" << Data << ", userid=" << m_userinfo.userid;

	Send(outbuf);
}

void ClientSession::OnUpdateUserInfoResponse(const std::string & data, const std::shared_ptr<TcpConnection>& conn)
{
	LOG_INFO << "update";
	bool updateSuccess = false;
	Json::Reader JsonReader;
	Json::Value JsonRoot;
	if (!JsonReader.parse(data, JsonRoot))
	{
		LOG_WARN << "invalid json: " << data << ", userid: " << m_userinfo.userid << ", client: " << conn->peerAddress().toHostPort();
		return;
	}

	if (!JsonRoot["nickname"].isString() || !JsonRoot["facetype"].isInt() ||
		!JsonRoot["customface"].isString() || !JsonRoot["gender"].isInt() ||
		!JsonRoot["birthday"].isInt() || !JsonRoot["signature"].isString() ||
		!JsonRoot["address"].isString() || !JsonRoot["phonenumber"].isString() ||
		!JsonRoot["mail"].isString())
	{
		LOG_WARN << "invalid json: " << data << ", userid: " << m_userinfo.userid << ", client: " << conn->peerAddress().toHostPort();
		return;
	}

	User newuserinfo;
	newuserinfo.nickname = JsonRoot["nickname"].asString();
	newuserinfo.facetype = JsonRoot["facetype"].asInt();
	newuserinfo.customface = JsonRoot["customface"].asString();
	newuserinfo.gender = JsonRoot["gender"].asInt();
	newuserinfo.birthday = JsonRoot["birthday"].asInt();
	newuserinfo.signature = JsonRoot["signature"].asString();
	newuserinfo.address = JsonRoot["address"].asString();
	newuserinfo.phonenumber = JsonRoot["phonenumber"].asString();
	newuserinfo.mail = JsonRoot["mail"].asString();

	string retData;
	if (!Singleton<UserManager>::Instance().UpdateUserInfo(m_userinfo.userid, newuserinfo))
	{
		Json::Value jsonObj;   // 构建对象
		jsonObj["code"] = 104;
		jsonObj["msg"] = "update userinfo failed!";
		Json::FastWriter writer;
		retData = writer.write(jsonObj);
	}
	else
	{
		/*
		{ "code": 0, "msg" : "ok", "userid" : 2, "username" : "xxxx",
		"nickname":"zzz", "facetype" : 26, "customface" : "", "gender" : 0, "birthday" : 19900101,
		"signature" : "xxxx", "address": "", "phonenumber": "", "mail":""}
		*/
		updateSuccess = true;

		Json::Value jsonObj;   // 构建对象
		jsonObj["code"] = 0;
		jsonObj["msg"] = "ok";
		jsonObj["userid"] = m_userinfo.userid;
		jsonObj["username"] = m_userinfo.username;
		jsonObj["nickname"] = newuserinfo.nickname;
		jsonObj["facetype"] = newuserinfo.facetype;
		jsonObj["customface"] = newuserinfo.customface;
		jsonObj["gender"] = newuserinfo.gender;
		jsonObj["birthday"] = newuserinfo.birthday;
		jsonObj["signature"] = newuserinfo.signature;
		jsonObj["address"] = newuserinfo.address;
		jsonObj["phonenumber"] = newuserinfo.phonenumber;
		jsonObj["mail"] = newuserinfo.mail;
		Json::FastWriter writer;
		retData = writer.write(jsonObj);
	}

	std::string outbuf;
	yt::BinaryWriteStream3 writeStream(&outbuf);
	writeStream.Write(msg_type_updateuserinfo);
	writeStream.Write(m_seq);
	writeStream.Write(retData.c_str(), retData.length());
	writeStream.Flush();

	//应答客户端
	Send(outbuf);

	LOG_INFO << "Response to client: cmd=msg_type_updateuserinfo, data=" << retData << ", userid=" << m_userinfo.userid;
	if (updateSuccess)
	{
		//给其他用户推送个人更新消息
		std::list<User> friends;
		Singleton<UserManager>::Instance().GetFriendInfoByUserId(m_userinfo.userid, friends);
		IMServer& imserver = Singleton<IMServer>::Instance();
		for (const auto& iter : friends)
		{
			//先看目标用户是否在线
			std::shared_ptr<ClientSession> targetSession;
			imserver.GetSessionByUserId(targetSession, iter.userid);
			if (targetSession.get() != nullptr)
				targetSession->SendUserStatusChangeMsg(m_userinfo.userid, 3);
		}
	}
}

//查找好友
void ClientSession::OnFindUserResponse(const std::string & data, const std::shared_ptr<TcpConnection>& conn)
{
	//{ "type": 1, "username" : "zhangyl" }
	Json::Reader JsonReader;
	Json::Value JsonRoot;
	if (!JsonReader.parse(data, JsonRoot))
	{
		LOG_WARN << "invalid json: " << data << ", userid: " << m_userinfo.userid << ", client: " << conn->peerAddress().toHostPort();
		return;
	}

	if (!JsonRoot["type"].isInt() || !JsonRoot["username"].isString())
	{
		LOG_WARN << "invalid json: " << data << ", userid: " << m_userinfo.userid << ", client: " << conn->peerAddress().toHostPort();
		return;
	}

	//TODO: 目前只支持查找单个用户
	string username = JsonRoot["username"].asString();
	User cachedUser;
	Json::Value jsonObj;
	Json::Value jsonArray;   // 构建对象
	if (!Singleton<UserManager>::Instance().GetUserInfoByUsername(username, cachedUser))
	{
		jsonObj["code"] = 0;
		jsonObj["msg"] = "ok";
		jsonObj["userinfo"] = jsonArray;
	}
	else
	{
		jsonObj["code"] = 0;
		jsonObj["msg"] = "ok";
		int i = 0;
		jsonArray[i]["userid"] = cachedUser.userid;
		jsonArray[i]["username"] = cachedUser.username;
		jsonArray[i]["nickname"] = cachedUser.nickname;
		jsonArray[i]["facetype"] = cachedUser.facetype;
		jsonObj["userinfo"] = jsonArray;
	}
	Json::FastWriter writer;
	std::string retData = writer.write(jsonObj);

	std::string outbuf;
	yt::BinaryWriteStream3 writeStream(&outbuf);
	writeStream.Write(msg_type_finduser);
	writeStream.Write(m_seq);
	writeStream.Write(retData.c_str(), retData.length());
	writeStream.Flush();

	LOG_INFO << "Response to client: cmd=msg_type_finduser, data=" << retData << ", userid=" << m_userinfo.userid;

	Send(outbuf);
}

//操作好友：添加，删除
void ClientSession::OnOperateFriendResponse(const std::string & data, const std::shared_ptr<TcpConnection>& conn)
{
	Json::Reader JsonReader;
	Json::Value JsonRoot;
	if (!JsonReader.parse(data, JsonRoot))
	{
		LOG_WARN << "invalid json: " << data << ", userid: " << m_userinfo.userid << ", client: " << conn->peerAddress().toHostPort();
		return;
	}

	if (!JsonRoot["type"].isInt() || !JsonRoot["userid"].isInt())
	{
		LOG_WARN << "invalid json: " << data << ", userid: " << m_userinfo.userid << ", client: " << conn->peerAddress().toHostPort();
		return;
	}

	int type = JsonRoot["type"].asInt();
	int32_t targetUserid = JsonRoot["userid"].asInt();
	if (targetUserid >= GROUPID_BOUBDARY)
	{
		if (4 == type)
		{
			//TODO:退群
		}
		//TODO:加群直接同意
		return;
	}

	//删除好友
	if (4 == type)
	{
		DeleteFriend(conn,targetUserid);
		return;
	}
	string retData;
	//发出加好友申请
	if (1 == type)
	{
		//判断是否已经是好友关系
		User myselfUser;
		if (!Singleton<UserManager>::Instance().GetUserInfoByUserId(m_userinfo.userid, myselfUser))
		{
			LOG_ERROR << "Get Userinfo by id error, targetuserid: " << m_userinfo.userid << ", userid: " << m_userinfo.userid << ", data: " << data << ", client: " << conn->peerAddress().toHostPort();
			return;
		}
		User targetUser;
		if (!Singleton<UserManager>::Instance().GetUserInfoByUserId(targetUserid, targetUser))
		{
			LOG_ERROR << "Get Userinfo by id error, targetuserid: " << targetUserid << ", userid: " << m_userinfo.userid << ", data: " << data << ", client: " << conn->peerAddress().toHostPort();
			return;
		}
		auto iter = myselfUser.friends.find(targetUserid);
		//已经是好友关系
		if (iter != myselfUser.friends.end())
		{
			Json::Value jsonObj;
			jsonObj["userid"] = targetUserid;
			jsonObj["type"] = 6;
			jsonObj["username"] = targetUser.username;

			Json::FastWriter writer;
			retData = writer.write(jsonObj);

			std::string outbufx;
			yt::BinaryWriteStream3 writeStream(&outbufx);
			writeStream.Write(msg_type_operatefriend);
			writeStream.Write(m_seq);
			writeStream.Write(retData.c_str(), retData.length());
			writeStream.Flush();

			Send(outbufx);
			LOG_INFO << "Response to client: cmd=msg_type_addfriend, data=" << retData << ", userid=" << m_userinfo.userid;
			return;
		}

		//{"userid": 9, "type": 1, }
		Json::Value jsonObj;
		jsonObj["userid"] = m_userinfo.userid;
		jsonObj["type"] = 2;
		jsonObj["username"] = m_userinfo.username;

		Json::FastWriter writer;
		retData = writer.write(jsonObj);
	}
	else if (3 == type)
	{
		if (!JsonRoot["accept"].isInt())
		{
			LOG_WARN << "invalid json: " << data << ", userid: " << m_userinfo.userid << "client: " << conn->peerAddress().toHostPort();
			return;
		}

		int accept = JsonRoot["accept"].asInt();
		//接受好友申请后，建立好友关系
		if (1 == accept)
		{
			int smallid = m_userinfo.userid;
			int greatid = targetUserid;
			//数据库里面互为好友的两个人id，小者在先，大者在后
			if (smallid > greatid)
			{
				smallid = targetUserid;
				greatid = m_userinfo.userid;
			}

			if (!Singleton<UserManager>::Instance().MakeFriendRelationship(smallid, greatid))
			{
				LOG_ERROR << "make relationship error: " << data << ", userid: " << m_userinfo.userid << "client: " << conn->peerAddress().toHostPort();
				return;
			}
		}

		Json::Value jsonObj;
		jsonObj["userid"] = m_userinfo.userid;
		jsonObj["type"] = 3;
		jsonObj["username"] = m_userinfo.username;
		jsonObj["accept"] = accept;
		
		//发给对方打包的消息
		Json::FastWriter writer;
		retData = writer.write(jsonObj);
		//提示自己当前用户加好友成功
		User targetUser;
		if (!Singleton<UserManager>::Instance().GetUserInfoByUserId(targetUserid, targetUser))
		{
			LOG_ERROR << "Get Userinfo by id error, targetuserid: " << targetUserid << ", userid: " << m_userinfo.userid << ", data: " << data << ", client: " << conn->peerAddress().toHostPort();
			return;
		}
		//打包发送给自己的数据包
		Json::Value json;
		json["userid"] = targetUserid;
		json["type"] = 3;
		json["username"] = targetUser.username;
		json["accept"] = accept;

		Json::FastWriter fwriter;
		string selfData = fwriter.write(json);

		std::string outbufx;
		yt::BinaryWriteStream3 writeStream(&outbufx);
		writeStream.Write(msg_type_operatefriend);
		writeStream.Write(m_seq);
		writeStream.Write(selfData.c_str(), selfData.length());
		writeStream.Flush();

		Send(outbufx);
		LOG_INFO << "Response to client: cmd=msg_type_addfriend, data=" << selfData << ", userid=" << m_userinfo.userid;
	}

	//提示对方好友添加成功
	std::string outbuf;
	yt::BinaryWriteStream3 writeStream(&outbuf);
	writeStream.Write(msg_type_operatefriend);
	writeStream.Write(m_seq);
	writeStream.Write(retData.c_str(), retData.length());
	writeStream.Flush();

	//先看目标用户是否在线
	std::shared_ptr<ClientSession> targetSession;
	Singleton<IMServer>::Instance().GetSessionByUserId(targetSession, targetUserid);
	//目标用户不在线，缓冲这个消息
	if (nullptr == targetSession.get())
	{
		Singleton<MsgCacheManager>::Instance().AddNotifyMsgCache(targetUserid, outbuf);
		LOG_INFO << "userid: " << targetUserid << " is not online, cache notify msg, msg: " << outbuf;
		return;
	}

	targetSession->Send(outbuf);

	LOG_INFO << "Response to client: cmd=msg_type_addfriend, data=" << data << ", userid=" << targetUserid;
}

//聊天内容直接转发给客户端就行，不用处理，留给客户端处理
void ClientSession::OnChatResponse(int32_t targetid, const std::string & data, const std::shared_ptr<TcpConnection>& conn)
{
	std::string outbuf;
	yt::BinaryWriteStream3 writeStream(&outbuf);
	writeStream.Write(msg_type_chat);
	writeStream.Write(m_seq);
	writeStream.Write(data.c_str(), data.length());
	//消息发送者
	writeStream.Write(m_userinfo.userid);
	//消息接受者
	writeStream.Write(targetid);
	writeStream.Flush();

	UserManager& userMgr = Singleton<UserManager>::Instance();
	
	//写入消息记录
	if (!userMgr.SaveChatMsgToDb(m_userinfo.userid, targetid, data))
	{
		LOG_ERROR << "Write chat msg to db error, , senderid = " << m_userinfo.userid << ", targetid = " << targetid << ", chatmsg:" << data;
	}

	IMServer& imserver = Singleton<IMServer>::Instance();
	MsgCacheManager& msgCacheMgr = Singleton<MsgCacheManager>::Instance();
	//单聊消息
	if (targetid < GROUPID_BOUBDARY)
	{
		//先看目标用户是否在线
		std::shared_ptr<ClientSession> targetSession;
		imserver.GetSessionByUserId(targetSession, targetid);
		//目标用户不在线，缓存这个消息
		if (targetSession.get() == nullptr)
		{
			msgCacheMgr.AddChatMsgCache(targetid, outbuf);
			return;
		}

		targetSession->Send(outbuf);
		return;
	}

	//TODO:群聊消息
}

void ClientSession::OnModifyPasswordResponse(const std::string & data, const std::shared_ptr<TcpConnection>& conn)
{
	Json::Reader JsonReader;
	Json::Value JsonRoot;
	if (!JsonReader.parse(data, JsonRoot))
	{
		LOG_WARN << "invalid json: " << data << ", client: " << conn->peerAddress().toHostPort();
		return;
	}

	if (!JsonRoot["oldpassword"].isString() || !JsonRoot["newpassword"].isString())
	{
		LOG_WARN << "invalid json: " << data << ", client: " << conn->peerAddress().toHostPort();
		return;
	}

	string username = JsonRoot["username"].asString();
	int userid = JsonRoot["userid"].asInt();
	string oldpass = JsonRoot["oldpassword"].asString();
	string newPass = JsonRoot["newpassword"].asString();

	string retdata;
	User cachedUser;
	if (!Singleton<UserManager>::Instance().GetUserInfoByUserId(userid, cachedUser))
	{
		LOG_ERROR << "get userinfo error, userid: " << userid << ", data: " << data << ", client: " << conn->peerAddress().toHostPort();
		return;
	}

	if (cachedUser.username.compare(username) != 0)
	{
		Json::Value jsonObj;
		jsonObj["code"] = 103;
		jsonObj["msg"] = "incorrect register phonenumber";

		Json::FastWriter writer;
		retdata = writer.write(jsonObj);
	}
	else if (cachedUser.password != oldpass)
	{
		Json::Value jsonObj;
		jsonObj["code"] = 104;
		jsonObj["msg"] = "incorrect old password";

		Json::FastWriter writer;
		retdata = writer.write(jsonObj);
	}
	else
	{
		if (!Singleton<UserManager>::Instance().ModifyUserPassword(userid, newPass))
		{
			Json::Value jsonObj;
			jsonObj["code"] = 105;
			jsonObj["msg"] = "modify password error";

			Json::FastWriter writer;
			retdata = writer.write(jsonObj);
			LOG_ERROR << "modify password error, userid: " << userid << ", data: " << data << ", client: " << conn->peerAddress().toHostPort();
		}
		else
		{
			Json::Value jsonObj;
			jsonObj["code"] = 0;
			jsonObj["msg"] = "ok";

			Json::FastWriter writer;
			retdata = writer.write(jsonObj);
		}
	}

	std::string outbuf;
	yt::BinaryWriteStream3 writeStream(&outbuf);
	writeStream.Write(msg_type_modifypassword);
	writeStream.Write(m_seq);
	writeStream.Write(retdata.c_str(), retdata.length());
	writeStream.Flush();

	//应答客户端
	Send(outbuf);

	LOG_INFO << "Response to client: cmd=msg_type_modifypassword, data=" << data << ", userid=" << userid;
}

void ClientSession::DeleteFriend(const std::shared_ptr<TcpConnection>& conn, int32_t friendid)
{
	int32_t smallerid = friendid;
	int32_t greaterid = m_userinfo.userid;
	if (smallerid > greaterid)
	{
		smallerid = m_userinfo.userid;
		greaterid = friendid;
	}

	if (!Singleton<UserManager>::Instance().ReleaseFriendRelationship(smallerid, greaterid))
	{
		LOG_ERROR << "Delete friend error, friendid: " << friendid << ", userid: " << m_userinfo.userid << ", client: " << conn->peerAddress().toHostPort();
		return;
	}

	User cachedUser;
	if (!Singleton<UserManager>::Instance().GetUserInfoByUserId(friendid, cachedUser))
	{
		LOG_ERROR << "Delete friend - Get user error, friendid: " << friendid << ", userid: " << m_userinfo.userid << ", client: " << conn->peerAddress().toHostPort();
		return;
	}

	//打包主动删除好友关系一方数据
	Json::Value jsonObj;
	jsonObj["userid"] = friendid;
	jsonObj["type"] = 5;
	jsonObj["username"] = cachedUser.username;

	Json::FastWriter writer;
	string retData = writer.write(jsonObj);

	std::string outbuf;
	yt::BinaryWriteStream3 writeStream(&outbuf);
	writeStream.Write(msg_type_operatefriend);
	writeStream.Write(m_seq);
	writeStream.Write(retData.data(), retData.length());
	writeStream.Flush();
	//发给主动删除一方
	Send(outbuf);
	LOG_INFO << "Send to client: cmd=msg_type_operatefriend, data=" << retData << ", userid=" << m_userinfo.userid;

	//发给被删除的一方
	if (friendid < GROUPID_BOUBDARY)
	{
		outbuf.clear();
		//先看目标用户是否在线
		std::shared_ptr<ClientSession> targetSesson;
		Singleton<IMServer>::Instance().GetSessionByUserId(targetSesson, friendid);
		//仅给在线用户推送这个消息
		if (targetSesson.get() != nullptr)
		{
			Json::Value jsonObj;
			jsonObj["userid"] = m_userinfo.userid;
			jsonObj["type"] = 5;
			jsonObj["username"] = m_userinfo.username;

			Json::FastWriter writer;
			string retData = writer.write(jsonObj);

			std::string outbuf;
			yt::BinaryWriteStream3 writeStream(&outbuf);
			writeStream.Write(msg_type_operatefriend);
			writeStream.Write(m_seq);
			writeStream.Write(retData.data(), retData.length());
			writeStream.Flush();
			targetSesson->Send(outbuf);

			LOG_INFO << "Send to client: cmd=msg_type_operatefriend, data=" << retData << ", userid=" << friendid;
		}

		return;
	}

	//TODO:退群消息
	//给其他在线群成员推送群信息发生变化的消息
}
