#pragma once

#include "mysqltask.h"
#include "MysqlThrd.h"

class CMysqlThrdMgr
{
public:
	CMysqlThrdMgr(void);
	~CMysqlThrdMgr(void);

public:
	bool Init(const string& host, const string& user, const string& pwd, const string& dbname);
	bool AddTask(uint32_t dwHashID, IMysqlTask* poTask);
	
	//FIXME:为什么要单独增加一个这个接口使用不参与轮询的那个线程
	bool AddTask(IMysqlTask* poTask)
	{
		return m_aoMysqlThreads[m_dwThreadsCount].AddTask(poTask);
	}
	
	//轮询线程
	inline uint32_t GetTableHashID(uint32_t dwHashID) const
	{
		return dwHashID % m_dwThreadsCount;			//FIXME:m_dwThreadsConut这个编号岂不是一直用不到
	}

	bool ProcessReplyTask(int32_t nCount);

	static uint32_t GetThreadsCount()
	{
		return m_dwThreadsCount;
	}
protected:
	static const uint32_t	m_dwThreadsCount = 9;
	CMysqlThrd				m_aoMysqlThreads[m_dwThreadsCount + 1];			//FIXME:为何写成加1的形式
};

